import { createStackNavigator } from 'react-navigation';
import Home from './screens/Home';
//account
import Profile from './screens/account/Profile';
import Register from './screens/account/Register';
import Dashboard from './screens/Dashboard';
//verb
import VerbFill from './screens/verb/VerbFill';
import Verbs from './screens/Verbs';
//sentence
import Sentence from './screens/sentence/Sentence';
import CompletedSentences from './screens/sentence/CompletedSentences';

const AppNavigator = 
createStackNavigator({
    Home: { screen: Home },
    Verbs: { screen: Verbs},
    VerbFill: { screen: VerbFill },
    Register: { screen: Register },
    Dashboard: { screen: Dashboard },
    Sentence: { screen: Sentence },
    Profile: { screen: Profile },
    CompletedSentences: { screen: CompletedSentences },
  });

export default AppNavigator;