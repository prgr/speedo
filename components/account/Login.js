import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import { AsyncStorage } from "react-native";

export default class Login extends React.Component {

    state = {
        username: '',
        password: '',
        showLoginError: false,
      }

      handleSignIn(){ 
        const user = {
            username: this.state.username,
            password: this.state.password,
        }
        fetch('http://localhost:3000/account/login', {
         method: 'post',
         headers: {'Content-Type':'application/json'},
         body: JSON.stringify(user)
        }).then((response) => response)
        .then((responseJson) => {
            if(responseJson.status == 200){
                console.log(responseJson);
                const responseUser = JSON.parse(responseJson['_bodyText']);
                this.setItem('user', responseUser._id);
                this.props.navigation.navigate('Dashboard');
            }
            else if (responseJson.status == 401){
                this.setState({
                    showLoginError: true
                })
            }
        })
        .catch((error) =>{
          console.error(error);
        });
    };

    async setItem(key, value) {
        try {
            console.log('--setting---');
            console.log(value);
            return await AsyncStorage.setItem(key, JSON.stringify(value));
        } catch (error) {
            // console.error('AsyncStorage#setItem error: ' + error.message);
        }
    }

  render() {
    return (
        <View style={styles.container}>
        <Image
            style={{height: 150, width: 150}}
            source={require('../../assets/speedo-logo.png')}
            resizeMode="contain"
        />
        <Text style={{ marginTop: 10, marginBottom: 25 }}>Learn Spanish. Learn quickly.</Text>
        <Text style={{ fontSize: 20, textTransform: "uppercase", marginBottom: 20, fontWeight: 'bold'}}>Login</Text>
        <Input
            style={styles.input}
            autoCapitalize = 'none'
            placeholder='Username'
            onChangeText={(value) => this.setState({ username: value})}
            leftIcon={
            <Icon
                name='person'
                size={24}
            />
            }/>
        <Input
            secureTextEntry={true}
            style={styles.input}
            autoCapitalize = 'none'
            placeholder='Password'
            onChangeText={(value) => this.setState({ password: value})}
            leftIcon={
            <Icon
                name='lock'
                size={24}
            />
        }/>
        { this.state.showLoginError ? 
        <Text style={styles.loginError}>
            Invalid username or password.
        </Text> : null } 
        <Button
            title='Sign in'
            buttonStyle={{
            backgroundColor: "rgba(92, 99,216, 1)",
            borderRadius: 20,
            width: 175,
            height: 45,
            marginTop: 20
            }}
            onPress={() => this.handleSignIn()}
        />
        <Text style={{ marginTop: 40}}>
            Do not have an account?
        </Text>
        <Button
            title='Sign up'
            buttonStyle={{
            backgroundColor: "rgba(92, 99,216, 1)",
            borderRadius: 20,
            width: 75,
            height: 45,
            marginTop: 10,
            }}
            onPress={() =>this.props.navigation.navigate('Register')}
        />
  </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fdfdfd',
      alignItems: 'center',
      justifyContent: 'center',
    },
    button: {
      backgroundColor: "rgba(92, 99,216, 1)",
      borderRadius: 20,
      height: 45,
      marginTop: 10,
    },
    input: {
      marginTop: 10
    },
    loginError: {
        marginTop: 10,
        color: 'red',
        fontWeight: 'bold',
    }
});