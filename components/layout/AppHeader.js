import React from 'react';
import { StyleSheet } from 'react-native';
import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AppHeader extends React.Component {

  state = {}

  componentDidMount(){
  }

  render() {
    return (
        <Header
            style={styles.header}
            placement="left"
            backgroundColor="white"
            leftComponent={{ icon: 'home', color: '#000' }}
            rightComponent={
                <Icon
                    name="user"
                    size={24}
                    styles={{ marginRight: 20 }}
                    onPress={() =>this.props.navigation.navigate('Profile')}
                /> 
            }
        />
    );
  }
}

const styles = StyleSheet.create({
    header: {
        padding: 20,
        marginTop: -105,
        height: 50
    }
});