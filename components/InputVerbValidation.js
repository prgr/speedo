import React from 'react';

export default class InputVerbValidation extends React.Component {

  state = {
    verbValidation: {
        firstPerson: null,
        secondPerson: null,
        thirdPerson: null,
        fourthPerson: null,
        fifthPerson: null,
        sixthPerson: null,
      }
  }


  handleChange(value, person) {
    value = value.toLowerCase();
    let currentState = this.state.verbValidation[person];
    const verbValidForm = this.state.verb[person];
    currentState = value === verbValidForm ? true : false
    this.setState({
        verbValidation: { ...this.state.verbValidation, [person]: currentState },
    });
  }

  handleIconChange = (person) =>{
    let color;
    switch (this.state.verbValidation[person]) {
      case false:
        color = 'red';
        break;
      case true: 
        color = 'green';
        break;
      default:
        color = '#ccc';
        break;
    }
    return color;
  }


  render() {
    return (
        <Input
        ref={input => { this.textInput6 = input }}
        style={styles.textInput}
        placeholder='Ellos/Ellas'
        onChangeText={(text) => this.handleChange(text, 'sixthPerson')}
        leftIcon={
          <Icon
            name={this.state.verbValidation['sixthPerson'] === null || this.state.verbValidation['sixthPerson'] === true ? 'check-circle' : 'times-circle'}
            size={24}
            color={this.handleIconChange('sixthPerson')}
          />
      }/>
    );
  }
}