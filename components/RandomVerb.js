import React from 'react';
import { Button } from 'react-native-elements';

export default class RandomVerb extends React.Component {

  state = {
  }

    handleClick = () => {
        let url = 'http://localhost:3000/verbs/random';
        return fetch(url)
          .then((response) => response.json())
          .then((responseJson) => {
                return responseJson.name
                console.log(responseJson);
          })
          .catch((error) =>{
            console.error(error);
          });
    }

  render() {
    return (
        <Button
            buttonStyle={{
                backgroundColor: "rgba(92, 99,216, 1)",
                borderRadius: 20,
                width: 175,
                height: 45,
                marginTop: 10,
            }}
            title='Get another one!'
            onPress={() =>
                this.props.navigation.navigate('VerbFill', {
                verb: this.handleClick(),
                })
            }
            />
    );
  }
}