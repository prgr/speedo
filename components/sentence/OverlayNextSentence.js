import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Overlay, Text, Button } from 'react-native-elements';


export default class OverlayNextSentence extends React.Component {

  state = {
    isVisible: true,
    greetings: ''
  }

  componentDidMount(){
    this.getGreetings();
  }

  getGreetings = () => {
      let url = 'http://localhost:3000/greetings/random';
      return fetch(url)
          .then((response) => response.json())
          .then((responseJson) => {
              this.setState({
                  greetings: responseJson.text
              })
          })
          .catch((error) =>{
          console.error(error);
      });
  }

  setSentencesAsChecked = () => {
    let url = 'http://localhost:3000/sentence/random';
    return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                greetings: responseJson.text
            })
        })
        .catch((error) =>{
        console.error(error);
    });
  }

  textPresentation(){
    const filledWords = this.props.filledWords;
    let filledWordsCount = 0;
    return this.props.text.split(' ').map((text, i) => {
          if(text === '###'){
            text = filledWords[filledWordsCount];
            filledWordsCount++;
          }
          const filledWordsStyles = {
            fontWeight: 'bold',
            fontSize: 22,
            textDecorationLine: 'underline',
          }
          return (
              <Text key={i} style={[{fontSize: 20}, this.props.filledWords.includes(text) ? filledWordsStyles : null]}> {text} </Text>
          )
    })
  }

  handleNextSentence(){
    // this.setState({
    //   isVisible: false,
    // })
    console.log('clicked');
    this.setState({
      isVisible: false
    })
    console.log('returned');
    return this.props.navigation.navigate('Sentence', {
      categoryId: '5bf722850a88bf09f5e0880e',
    })
  }

  render() {
    return (
      <View>
      <Overlay 
              isVisible={this.state.isVisible}
              windowBackgroundColor="rgba(0, 0, 0, 0.5)"
              overlayBackgroundColor="white"
              height={400}
              overlayStyle={styles.overlay}>
            <Text style={[styles.text, styles.greetings]}>{ this.state.greetings }</Text>
            <Text style={styles.text}>{this.textPresentation()}</Text>
            <Button
              title='Go get next sentence!'
              buttonStyle={{
              backgroundColor: "rgba(92, 99,216, 1)",
              borderRadius: 20,
              width: 200,
              height: 45,
              marginTop: 30
              }}
              color='#000'
              onPress={() => this.handleNextSentence()}
            />
        </Overlay>
  </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    padding: 50,
    marginBottom: 100,
  },
  overlay: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  greetings: {
      fontSize: 30,
      fontWeight: 'bold',
      color: '#1bb2ff',
      marginTop: 20,
      marginBottom: 20,
  },
  text: {
    textAlign: 'center',
    color: 'green'
  }
});