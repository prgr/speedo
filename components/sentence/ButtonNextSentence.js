import React from 'react';
import { Button } from 'react-native-elements';
import AppHeader from '../layout/AppHeader';

export default class ButtonNextSentence extends React.Component {

  state = {

  }

  componentDidMount(){
  }

  render() {
    return (
        <Button
        title='Go get next sentence!'
        buttonStyle={{
        backgroundColor: "rgba(92, 99,216, 1)",
        borderRadius: 20,
        width: 200,
        height: 45,
        marginTop: 30
        }}
        color='#000'
        onPress={() => this.props.onButtonClick()}
      />
    );
  }
}