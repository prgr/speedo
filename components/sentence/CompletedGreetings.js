import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';


export default class CompletedGreetings extends React.Component {

  state = {
    greetings: ''
  }

  componentDidMount(){
    this.getGreetings();
  }

  getGreetings = () => {
      let url = 'http://localhost:3000/greetings/random';
      return fetch(url)
          .then((response) => response.json())
          .then((responseJson) => {
              this.setState({
                  greetings: responseJson.text
              })
          })
          .catch((error) =>{
          console.error(error);
      });
  }

  textPresentation(){
    const filledWords = this.props.filledWords;
    let filledWordsCount = 0;
    return this.props.text.split(' ').map((text, i) => {
          if(text === '###'){
            text = filledWords[filledWordsCount];
            filledWordsCount++;
          }
          const filledWordsStyles = {
            fontWeight: 'bold',
            fontSize: 22,
            textDecorationLine: 'underline',
          }
          return (
              <Text key={i} style={[{fontSize: 20}, this.props.filledWords.includes(text) ? filledWordsStyles : null]}> {text} </Text>
          )
    })
  }

  render() {
    return (
      <View>
        <Text style={[styles.text, styles.greetings]}>{ this.state.greetings }</Text>
        <Text style={styles.text}>{this.textPresentation()}</Text>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    padding: 50,
    marginBottom: 100,
  },
  greetings: {
      fontSize: 30,
      fontWeight: 'bold',
      color: '#1bb2ff',
      marginTop: 20,
      marginBottom: 20,
  },
  text: {
    textAlign: 'center',
    color: 'green'
  }
});