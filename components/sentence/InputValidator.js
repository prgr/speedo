import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

export default class InputValidator extends React.Component {

  constructor(props) {
    super(); // or super(props) ?
    this.state = {
      spanishWords: [],
      fieldsMeaning: [],
      userFields: [],
      validFields: [],
    }
  }



  componentDidMount(){
    this.setState({
        spanishWords: this.props.sentence.fields.split(','),
        fieldsMeaning: this.props.sentence.fieldsMeaning.split(','),
    });
    const fieldsLength = this.props.sentence.fields.split(',').length
    const userFields = [...this.state.userFields];
    for (let i = 0; i < fieldsLength; i++) {
        userFields.push(null);
    }
    this.setState({ userFields });
  }

  handleInputBorderColorChange = (text, count) =>{
    text = text.toLowerCase();
    const userFields = [...this.state.userFields];
    switch (text == this.state.spanishWords[count]) {
      case false:
        userFields[count] = false;
        break;
      case true: 
        userFields[count] = true;
        break;
      default:
        userFields[count] = null;
        break;
    }
    this.setState({ userFields });
  }

  setInputBorderColor(){
    color = 'transparent';
    switch (this.props.userField) {
        case false:
          color = 'red';
          break;
        case true: 
          color = 'green';
          break;
        default:
          color = 'transparent';
          break;
      }
      return color;
  }

  render() {
    return (
        <TextInput
            style={[styles.input, {borderBottomColor: this.setInputBorderColor()}]}
            autoCapitalize = 'none'
            placeholder={this.state.fieldsMeaning[this.props.count]}
            onChangeText={(text) => this.props.onInputChange(text, this.props.count)}
        />
    );
  }
}

const styles = StyleSheet.create({
    input: {
        width: 100,
        fontSize: 20,
        marginTop: -10,
        borderBottomWidth: 2,
        borderBottomColor: 'grey',
    },
    borderSuccess: {
        borderBottomColor: 'green',
    },
    borderAlert: {
        borderBottomColor: 'red',
    }
});