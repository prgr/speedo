import React from 'react';
import { StyleSheet } from 'react-native';

export default class RandomVerb extends React.Component{

    state = {
        verb: null,
    }

    handleRandomVerb(){
        let url = 'http://localhost:3000/verbs/random';
        return fetch(url)
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({
                  verb: responseJson
                })
          })
          .catch((error) =>{
            console.error(error);
          });
      }

    render(){
        return(
            <Button
            buttonStyle={{
              backgroundColor: "rgba(92, 99,216, 1)",
              borderRadius: 20,
              width: 175,
              height: 45,
              marginTop: 10,
            }}
            title='Get another one!'
            onPress={() =>
              this.props.navigation.navigate('VerbFill', {
                verb: this.handleRandomVerb(),
              })
            }
            />
        )
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fdfdfd',
      alignItems: 'center',
      justifyContent: 'center',
    },
});