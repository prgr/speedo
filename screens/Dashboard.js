import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { ListItem } from 'react-native-elements';
import AppHeader from '../components/layout/AppHeader';

export default class Dashboard extends React.Component {

  state = {
    categories: [],
    verb: {},
  }

  componentDidMount(){
      this.getCategories();
      this.handleRandomVerb();
  }

  getCategories = () =>{
    let url = 'http://localhost:3000/categories';
    return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            this.setState({
                categories: responseJson
            })
        })
        .catch((error) =>{
        console.error(error);
    });
  }

  cardCategories(){
    return this.state.categories.map((category, i) => {
      return (
              <ListItem
                key={i}
                title={category.name}
                leftIcon={{ name: category.icon }}
                onPress={() =>
                  this.props.navigation.navigate('Sentence', {
                    categoryId: category._id,
                  })
                }
              />
      )
    });
  }

  handleRandomVerb(){
    let url = 'http://localhost:3000/verbs/random';
    return fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
            this.setState({
              verb: responseJson
            });
      })
      .catch((error) =>{
        console.error(error);
      });
  }

  render() {
    return (
    <React.Fragment>
        <AppHeader navigation={this.props.navigation} />
        <View style={styles.container}>
      <Text style={styles.heading}>Sentences categories</Text>
      <View>{this.cardCategories()}</View>
      <Text style={styles.heading}>Verbs</Text>
      <ListItem
            title='Conjugation'
            leftIcon={{ name: 'create' }}
            onPress={() =>
              this.props.navigation.navigate('VerbFill', {
                verb: this.state.verb
              })
            }
        />
    </View>
    </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    padding: 10,
  },
  heading: {
    fontSize: 20,
    textTransform: 'uppercase',
    marginTop: 10,
    marginBottom: 10,
    fontWeight: 'bold',
  }
});