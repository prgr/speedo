import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button } from 'react-native-elements';
import { AsyncStorage } from "react-native";
import Login from '../components/account/Login';

export default class Home extends React.Component {

  state = {
    isAuthenticated: null
  }

  handleIsAuthenticated = async () => {
      try {
        const value = await AsyncStorage.getItem('user');
        if (value !== null) {
          this.setState({ isAuthenticated: true })
        }
        else{
          //this.setState({ isAuthenticated: true })
          this.props.navigation.navigate('Dashboard')
        }
       } catch (error) {
         // Error retrieving data
       }
       console.log(this.state.isAuthenticated);
    }

  componentDidMount(){
      this.clearAsyncStorage();
      this.handleIsAuthenticated();
  }

  clearAsyncStorage = async() => {
    AsyncStorage.clear();
  }

  render() {
    let view;
    if(this.state.isAuthenticated){
      view = 
      <Button
      buttonStyle={{
        backgroundColor: "rgba(92, 99,216, 1)",
        borderRadius: 20,
        height: 45,
      }}
      title='Choose verb from the list'
      onPress={() =>
        this.props.navigation.navigate('Dashboard', {
          verb: null,
        })
      }
      />
    }
    else{
      view = <Login navigation={this.props.navigation} />
    }

    return (
      <View style={styles.container}>
          { view }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdfdfd',
  },
  button: {
    backgroundColor: "rgba(92, 99,216, 1)",
    borderRadius: 20,
    height: 45,
    marginTop: 10,
  }
});