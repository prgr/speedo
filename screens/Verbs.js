import React from 'react';
import { StyleSheet, Text, View, Picker, Image, TouchableHighlight, Switch, StatusBar, TextInput } from 'react-native';
import { Button } from 'react-native-elements';

export default class Friends extends React.Component {

    state = {
        dataSource: [],
        selectedVerb: null,
        selectedLang: 'spain',
      }


    componentDidMount(){
        let url = 'http://localhost:3000/verbs';
        return fetch(url)
          .then((response) => response.json())
          .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    dataSource: responseJson,
                    selectedVerb: responseJson[0]._id
                  }, function(){
                });
          })
          .catch((error) =>{
            console.error(error);
          });
      }

    handleLanguageClick = (lang) => (evt) => {
          this.setState({
            selectedLang : lang
          })
    }
    
    
  render() {

    let serviceItems = this.state.dataSource.map( (verb, i) => {
      return <Picker.Item key={i} value={verb._id} label={this.state.selectedLang === 'spain' ? verb.spanishWord : verb.name} />
    });
      
    return (
    <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
        {/* <Text onPress={this.handleLanguageClick('en')} style={styles.languages}>
            EN
        </Text> */}
        {/* <TouchableHighlight onPress={this.handleLanguageClick('en')} style={ { margin: 10 }}>
          <Image
              style={{height: 50, width: 50}}
              source={require('../assets/flags/flag-en.png')}
              resizeMode="contain"
            />
        </TouchableHighlight> */}
        <TouchableHighlight onPress={this.handleLanguageClick('spain')} style={ { margin: 10 }}>
          <Image
              style={{height: 50, width: 50}}
              source={require('../assets/flags/flag-es.png')}
              resizeMode="contain"
            />
        </TouchableHighlight>
        {/* <Text onPress={this.handleLanguageClick('spain')} style={styles.languages}>
            ES
        </Text> */}
        </View>
        <Picker
                selectedValue={this.state.selectedVerb}
                style={{ height: 300, width: '100%' }}
                onValueChange={(itemValue, itemIndex) => this.setState({selectedVerb: itemValue})}>
                { serviceItems }
        </Picker>
        <Button
            title='Check yourself'
            buttonStyle={{
              backgroundColor: "rgba(92, 99,216, 1)",
              borderRadius: 20,
              width: 175,
              height: 45,
            }}
            onPress={() =>
              this.props.navigation.navigate('VerbFill', {
                verb: this.state.selectedVerb,
              })
            }
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4e2d8',
    alignItems: 'center',
    justifyContent: 'center',
  },
  languages: {
    fontSize: 30,
    padding: 5,
  }
});