import React from 'react';
import { StyleSheet, Text, View, Picker, Switch, StatusBar, TextInput } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import AppHeader from '../../components/layout/AppHeader';

export default class VerbFill extends React.Component {

    state = {
        verb: {},
        verbValidation: {
          firstPerson: null,
          secondPerson: null,
          thirdPerson: null,
          fourthPerson: null,
          fifthPerson: null,
          sixthPerson: null,
        }
    }

      componentDidMount(){
        this.initialState = this.state;
        const { navigation } = this.props;
        let verb = navigation.getParam('verb');
        let url = 'http://localhost:3000/verbs/' + verb._id;
        return fetch(url)
          .then((response) => response.json())
          .then((responseJson) => {
                this.setState({
                  verb: responseJson
                })
          })
          .catch((error) =>{
            console.error(error);
          });
      }

    handleChange(value, person) {
      value = value.toLowerCase();
      let currentState = this.state.verbValidation[person];
      const verbValidForm = this.state.verb[person];
      currentState = value === verbValidForm ? true : false
      this.setState({
          verbValidation: { ...this.state.verbValidation, [person]: currentState },
      });
    }

    handleIconChange = (person) =>{
      let color;
      switch (this.state.verbValidation[person]) {
        case false:
          color = 'red';
          break;
        case true: 
          color = 'green';
          break;
        default:
          color = '#ccc';
          break;
      }
      return color;
    }

    resetInputsValues = () => {
        this.textInput1.clear();
        this.textInput2.clear();
        this.textInput3.clear();
        this.textInput4.clear();
        this.textInput5.clear();
        this.textInput6.clear();
    }

    resetVerbsValidation = () => {

    }

    handleRandomVerb(){
      this.setState(this.initialState)
      this.resetInputsValues();
      console.log('clicked');
      let url = 'http://localhost:3000/verbs/random';
      return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
              this.setState({
                verb: responseJson
              })
              console.log(responseJson);
        })
        .catch((error) =>{
          console.error(error);
        });
    }
     
  render() {
      
    return (
      <React.Fragment>
        <AppHeader />
      <View style={styles.container}>
          <Text style={styles.mainVerb}> { this.state.verb.spanishWord } ({ this.state.verb.name }) </Text>
          <View style={styles.textInputWrapper}>
              <Input
                ref={input => { this.textInput1 = input }}
                style={styles.textInput}
                placeholder='Yo'
                onChangeText={(text) => this.handleChange(text, 'firstPerson')}
                leftIcon={
                  <Icon
                    name={this.state.verbValidation['firstPerson'] === null || this.state.verbValidation['firstPerson'] === true ? 'check-circle' : 'times-circle'}
                    size={24}
                    color={this.handleIconChange('firstPerson')}
                  />
              }/>
              <Text style={styles.margin}></Text>
              <Input
                ref={input => { this.textInput2 = input }}
                style={styles.textInput}
                placeholder='El/Ella'
                onChangeText={(text) => this.handleChange(text, 'secondPerson')}
                leftIcon={
                  <Icon
                    name={this.state.verbValidation['secondPerson'] === null || this.state.verbValidation['secondPerson'] === true ? 'check-circle' : 'times-circle'}
                    size={24}
                    color={this.handleIconChange('secondPerson')}
                  />
              }/>
        </View>
        <View style={styles.textInputWrapper}>
              <Input
                ref={input => { this.textInput3 = input }}
                style={styles.textInput}
                placeholder='Tu'
                onChangeText={(text) => this.handleChange(text, 'thirdPerson')}
                leftIcon={
                  <Icon
                    name={this.state.verbValidation['thirdPerson'] === null || this.state.verbValidation['thirdPerson'] === true ? 'check-circle' : 'times-circle'}
                    size={24}
                    color={this.handleIconChange('thirdPerson')}
                  />
              }/>
              <Text style={styles.margin}></Text>
              <Input
                ref={input => { this.textInput4 = input }}
                style={styles.textInput}
                placeholder='Vosotros/as'
                onChangeText={(text) => this.handleChange(text, 'fourthPerson')}
                leftIcon={
                  <Icon
                    name={this.state.verbValidation['fourthPerson'] === null || this.state.verbValidation['fourthPerson'] === true ? 'check-circle' : 'times-circle'}
                    size={24}
                    color={this.handleIconChange('fourthPerson')}
                  />
              }/>
        </View>
        <View style={styles.textInputWrapper}>
              <Input
                ref={input => { this.textInput5 = input }}
                style={styles.textInput}
                placeholder='Nosotros/as'
                onChangeText={(text) => this.handleChange(text, 'fifthPerson')}
                leftIcon={
                  <Icon
                    name={this.state.verbValidation['fifthPerson'] === null || this.state.verbValidation['fifthPerson'] === true ? 'check-circle' : 'times-circle'}
                    size={24}
                    color={this.handleIconChange('fifthPerson')}
                  />
              }/>
              <Text style={styles.margin}></Text>
              <Input
                ref={input => { this.textInput6 = input }}
                style={styles.textInput}
                placeholder='Ellos/Ellas'
                onChangeText={(text) => this.handleChange(text, 'sixthPerson')}
                leftIcon={
                  <Icon
                    name={this.state.verbValidation['sixthPerson'] === null || this.state.verbValidation['sixthPerson'] === true ? 'check-circle' : 'times-circle'}
                    size={24}
                    color={this.handleIconChange('sixthPerson')}
                  />
              }/>
        </View>
        <Button
          buttonStyle={{
            backgroundColor: "rgba(92, 99,216, 1)",
            borderRadius: 20,
            width: 175,
            height: 45,
            marginTop: 10,
          }}
          title='Get another one!'
          onPress={() =>
            this.props.navigation.navigate('VerbFill', {
              verb: this.handleRandomVerb(),
            })
          }
          />
      </View>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInputWrapper: {
    flexDirection: 'row',
    paddingLeft: 110,
    paddingRight: 110,
    paddingBottom: 10,
  },
  textInput: {
    flex: 1,
  },
  margin: {
    margin: 10
  },
  mainVerb: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 50
  }
});