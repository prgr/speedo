import React from 'react';
import { StyleSheet, View, AsyncStorage, } from 'react-native';
import { Text, Overlay, Button } from 'react-native-elements';
import InputValidator from '../../components/sentence/InputValidator';
import OverlayNextSentence from '../../components/sentence/OverlayNextSentence';
import AppHeader from '../../components/layout/AppHeader';
import CompletedGreetings from '../../components/sentence/CompletedGreetings';
import ButtonNextSentence from '../../components/sentence/ButtonNextSentence';

export default class SentencesTwo extends React.Component {

  state = {
    sentence: null,
    text: '',
    spanishWords: [],
    userFields: [],
    showOverlay: false,
  }

  componentDidMount(){
    console.log('hero');
      this.setState({
        showOverlay: false,
      })
      this.getSentence();
  }

  getSentence = () =>{
    const { navigation } = this.props;
    const categoryId = navigation.getParam('categoryId');
    let url = `http://localhost:3000/sentences?checked=false&categoryId=${categoryId}&random=true`;
    return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                sentence: responseJson,
                text: responseJson.text,
                spanishWords: responseJson.fields.split(',')
            }, () => {
              const fieldsLength = this.state.spanishWords.length
              const userFields = [...this.state.userFields];
              for (let i = 0; i < fieldsLength; i++) {
                  userFields.push(null);
              }
              this.setState({ userFields });
            });
        })
        .catch((error) =>{
          console.error(error);
    });
  }

  handleInputChange = (text, count) => {
    text = text.toLowerCase();
    const userFields = [...this.state.userFields];
    switch (text == this.state.spanishWords[count]) {
      case false:
        userFields[count] = false;
        break;
      case true: 
        userFields[count] = true;
        break;
      default:
        userFields[count] = null;
        break;
    }
    this.setState({ userFields }, () => {
      this.showOverlay();
    });
  }

  showOverlay = () => {
    const userFields = [...this.state.userFields]
      if(!(userFields.includes(false) || userFields.includes(null))){
          this.setSentenceAsCompleted();
          this.setState({
            showOverlay: true
          })
      }
  }

  setSentenceAsCompleted = () => {
      const formData = new FormData();
      AsyncStorage.getItem("user").then((value) => {
        formData.append('_id', value);
      });
      formData.append('sentenceId', this.state.sentence._id);
      return fetch('http://localhost:3000/account/', {
          method: 'PUT',
          body: formData
      })
      .then((response) => {
        console.log(response.status);
      })
      .catch((error) =>{
        console.error(error);
    });
  }

  textInputs(){
    let inputCount = -1;
    return this.state.text.split(' ').map((text, i) => {
        if(text !== '###'){
            return (
                <Text key={i} style={{fontSize: 20, marginBottom: 15}}> {text} </Text>
            )
        }
        else{
            inputCount++;
            return(
                <InputValidator 
                key={i} 
                sentence={this.state.sentence} 
                onInputChange={this.handleInputChange}
                userField={this.state.userFields[inputCount]}
                count={inputCount} />
            )
        }
    })
  }

  componentWillMount() {
    this.initialState = this.state
  }

  handleNextSentence = () =>{
    console.log('hero');
    this.setState(this.initialState);
    console.log('charlie');
    this.getSentence();
  }

  render() {

    if(this.state.showOverlay){
      return(

        <Overlay 
        isVisible={true}
        windowBackgroundColor="rgba(0, 0, 0, 0.5)"
        overlayBackgroundColor="white"
        height={400}
        overlayStyle={styles.overlay}>
                <CompletedGreetings text={this.state.text} filledWords={this.state.spanishWords} />
                <ButtonNextSentence onButtonClick={this.handleNextSentence} />
  </Overlay>
      )
    }

    return (
      <React.Fragment>
          <AppHeader navigation={this.props.navigation} />
          <View style={styles.container}> 
              <View style={{flexDirection: 'row', flexWrap: 'wrap', alignSelf: 'flex-start'}}>
                    { this.textInputs()}
              </View>
          </View>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdfdfd',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 25,
  },
  button: {
    backgroundColor: "rgba(92, 99,216, 1)",
    borderRadius: 20,
    height: 45,
    marginTop: 10,
  },
  input: {
    width: 100,
    fontSize: 20,
  },
  heading: {
    fontSize: 22,
    textTransform: 'uppercase',
    marginTop: 10,
    marginBottom: 10,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  overlay: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
