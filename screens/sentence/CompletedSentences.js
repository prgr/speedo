import React from 'react';
import { StyleSheet, View, Text, AsyncStorage } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';
import AppHeader from '../../components/layout/AppHeader';

export default class CompletedSentences extends React.Component {

  state = {
    categories: [],
    verb: {},
    isAuthenticated: '',
    user: {},
    completedSentences: [],
  }

  componentDidMount(){
      AsyncStorage.getItem("user").then((value) => {
        console.log(value);
        this.setState({isAuthenticated: JSON.parse(value)});
      })
      .then(res => {
          this.handleIsAuthenticated();
      });
  }

  handleIsAuthenticated = () => {
    const auth = this.state.isAuthenticated;
    let url = 'http://localhost:3000/account/' + auth;
    return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
            this.setState({
                user: responseJson.user,
                completedSentences: responseJson.user.completedSentences,
                totalSentences: responseJson.totalSentences,
            })
        })
        .catch((error) =>{
        console.error(error);
    });
  }

   cardCategories(){
    return this.state.completedSentences.map((sentence, i) => {
      return (
              <ListItem
                key={i}
                title={sentence.text + sentence.text}
              />
      )
    });
  }

  render() {
    return (
    <React.Fragment>
        <AppHeader navigation={this.props.navigation} />
        <View style={styles.container}>
            <Text style={styles.heading}>Completed sentences {this.state.completedSentences.length} / { this.state.totalSentences }.</Text>
            <Text>Reset</Text>
            <Icon
  reverse
  name='clear-all'
  color='#517fa4'
/>
            { this.cardCategories() }
        </View>
    </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    padding: 10,
  },
  heading: {
    fontSize: 20,
    textTransform: 'uppercase',
    marginTop: 10,
    marginBottom: 10,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'green'
  },
});