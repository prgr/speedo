import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { ListItem } from 'react-native-elements';

export default class Profile extends React.Component {

  state = {
  }

  componentDidMount(){
      
  }

  getCategories = () =>{
    let url = 'http://localhost:3000/categories';
    return fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                categories: responseJson
            })
        })
        .catch((error) =>{
        console.error(error);
    });
  }

  render() {
    return (
    <View style={styles.container}>
      <Text style={styles.heading}>Statistics</Text>
      <ListItem
            title='Completed sentences'
            leftIcon={{ name: 'create' }}
            onPress={() =>
              this.props.navigation.navigate('CompletedSentences')
            }
        />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    padding: 10,
  },
  heading: {
    fontSize: 20,
    textTransform: 'uppercase',
    marginTop: 10,
    marginBottom: 10,
    fontWeight: 'bold',
  }
});