import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';

export default class Home extends React.Component {

  state = {
    username: '',
    email: '',
    password: '',
    requiredError: false
  }

handleSignUp(){ 
    const user = {
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
    }
    if(user.username == '' || user.email == '' || user.password == ''){
        this.setState({
            requiredError: true
        })
    }
    fetch('http://localhost:3000/account/register', {
     method: 'post',
     headers: {'Content-Type':'application/json'},
     body: JSON.stringify(user)
    }).then((response) => {
        if(response.status == 200){
            this.props.navigation.navigate('Verbs');
        }
    })
    .catch((error) =>{
      console.error(error);
    });
};

  render() {
    return (
      <View style={styles.container}>
            <Image
                style={{height: 150, width: 150}}
                source={require('../../assets/speedo-logo.png')}
                resizeMode="contain"
            />
            <Text style={{ marginTop: 10, marginBottom: 25 }}>Sign up to learn spanish fast and efficiently.</Text>
            <Text style={{ fontSize: 20, textTransform: "uppercase", marginBottom: 20, fontWeight: 'bold'}}>Register</Text>
            <Input
                style={styles.input}
                autoCapitalize = 'none'
                placeholder='Username'
                onChangeText={(value) => this.setState({ username: value})}
                leftIcon={
                <Icon
                    name='person'
                    size={24}
                />
                }/>
            <Input
                style={styles.input}
                placeholder='Email'
                autoCapitalize = 'none'
                onChangeText={(value) => this.setState({ email: value})}
                leftIcon={
                <Icon
                    name='email'
                    size={24}
                />
                }/>
            <Input
                style={styles.input}
                secureTextEntry={true}
                autoCapitalize = 'none'
                placeholder='Password'
                onChangeText={(value) => this.setState({ password: value})}
                leftIcon={
                <Icon
                    name='lock'
                    size={24}
                />
            }/>
            { this.state.requiredError ? 
            <Text style={styles.requiredError}>
                All fields are required.
            </Text> : null } 
            <Button
                title='Sign up'
                buttonStyle={{
                backgroundColor: "rgba(92, 99,216, 1)",
                borderRadius: 20,
                width: 175,
                height: 45,
                marginTop: 20
                }}
                onPress={() => this.handleSignUp()
            }
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdfdfd',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: "rgba(92, 99,216, 1)",
    borderRadius: 20,
    height: 45,
    marginTop: 10,
  },
  input: {
    marginTop: 10
  },
  requiredError: {
    marginTop: 10,
    color: 'red',
    fontWeight: 'bold',
    },
});