import React from 'react';
import { StyleSheet, Text, View, Switch, StatusBar, TextInput } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import AppNavigator from './AppNavigator';


export default class App extends React.Component {


  render() {
    return (
      <React.Fragment>
        <AppNavigator/>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'hotpink',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
